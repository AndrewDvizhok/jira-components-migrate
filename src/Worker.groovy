import groovy.json.JsonSlurper

import javax.net.ssl.HostnameVerifier
import javax.net.ssl.HttpsURLConnection
import javax.net.ssl.SSLContext
import javax.net.ssl.SSLSession
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager
import javax.swing.*
import java.awt.GridLayout
import java.awt.event.ActionEvent
import java.awt.event.ActionListener
import java.security.cert.CertificateException;

class Worker implements Migrate {

    private credentials
    private account
    private url
    private disableCert
    private fromProjectKey
    private toProjectKey
    private lastResponseStatus

    @Override
    Set getComponents(Object projectKey) {
        def components = [] as Set
        try{
        new JsonSlurper().parseText(
                getRequest(url + "/rest/api/latest/project/${projectKey}/components")
        ).each {
            if (it == null) {
                throw new NullPointerException('API return null project!')
            }
            JiraComponent jiraComponent = new JiraComponent(it.id, it.name, it.assigneeType)
            if (it.description != null) {
                jiraComponent.description = it.description
            }
            if (it.lead != null) {
                jiraComponent.lead = it.lead.name
            }
            components.add(jiraComponent)
        }
        }catch(Exception e){
            println "error on ${projectKey}"
            println e.getMessage()
            e.getStackTrace().each { println it }
        }
        return components
    }

    @Override
    def compareComponents(Object leftProject, Object rightProject) {
        fromProjectKey = leftProject
        toProjectKey = rightProject
        def date = new Date()
        File compareCompFile = new File("${fromProjectKey}-${toProjectKey}-${date.getTime()}.csv")
        compareCompFile.write("Name-${fromProjectKey},Desc,Lead,Assignee,Name-${toProjectKey},Desc,Lead,Assignee\n")
        def fromComps = getComponents(fromProjectKey)
        def toComps = getComponents(toProjectKey)
        fromComps.each { fromComponent ->
            if (toComps.find { toComponent -> toComponent.name == fromComponent.name }) {
                def toComponent = toComps.find { toComponent -> toComponent.name == fromComponent.name }
                compareCompFile.append("${fromComponent},${toComponent}\n")
            } else {
                compareCompFile.append("${fromComponent}\n")
            }
        }
        toComps.each { toComponent ->
            if (!fromComps.find { fromComponent -> fromComponent.name == toComponent.name }) {
                compareCompFile.append(",,,,${toComponent}\n")
            }
        }

        return [from: fromComps, to: toComps]
    }

    @Override
    def unionComponentsNoReplace(Object fromProject, Object toProject) {
        def components = compareComponents(fromProject, toProject)
        components.from.each { JiraComponent fromComponent ->
            JiraComponent toComponent = components.to.find { toCopmonent -> toCopmonent.name == fromComponent.name }
            if (toComponent == null) {
                createComponent(fromComponent)
            }
        }
        components
    }

    @Override
    def unionComponentsWithReplace(Object fromProject, Object toProject) {
        def components = unionComponentsNoReplace(fromProject, toProject)
        components.from.each { JiraComponent fromComponent ->
            JiraComponent toComponent = components.to.find { toCopmonent -> toCopmonent.name == fromComponent.name }
            if (toComponent != null) {
                updateComponent(fromComponent, toComponent)
            }
        }
        components
    }

    @Override
    def syncComponents(Object fromProject, Object toProject) {
        def components = unionComponentsWithReplace(fromProject, toProject)
        components.to.each { JiraComponent toComponent ->
            if (!components.from.find { fromCopmonent -> fromCopmonent.name == toComponent.name }) {
                deleteComponent(toComponent)
            }
        }
        components
    }

    void askParameters() {

        def frame = new JFrame("Parameters")

        frame.setLocationRelativeTo(null)

        def lblUrl = new JLabel("Jira URL")
        def jiraUrl = new JTextField(20)
        lblUrl.setLabelFor(jiraUrl)

        def lblUser = new JLabel("User Name:");
        def tfUser = new JTextField(20);
        lblUser.setLabelFor(tfUser);

        def lblPassword = new JLabel("Password:");
        def final pfPassword = new JPasswordField(20);
        lblPassword.setLabelFor(pfPassword);

        JButton btnGet = new JButton("Display Password");
        btnGet.addActionListener(
                new ActionListener() {

                    void actionPerformed(ActionEvent e) {
                        def password = new String(pfPassword.getPassword());
                        JOptionPane.showMessageDialog(frame,
                                "Password is " + password);
                    }
                });

        def btnLogin = new JButton("Login");

        def lblCert = new JLabel("Disable check certificate");
        def cbCert = new JCheckBox();
        lblCert.setLabelFor(cbCert);

        btnLogin.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {

                if (jiraUrl.getText().lastIndexOf("/") == jiraUrl.getText().length() - 1) {
                    url = jiraUrl.getText().substring(0, jiraUrl.getText().length() - 1);
                } else {
                    url = jiraUrl.getText();
                }

                account = tfUser.getText();
                credentials = "${tfUser.getText()}:${pfPassword.getPassword()}".getBytes().encodeBase64().toString();
                disableCert = cbCert.isSelected()
                if (checkConnection()) {
                    frame.setVisible(false)
                    askFromToProjects()
                } else {
                    JOptionPane.showMessageDialog(frame, "Can't login! Please check login/name or try trust all cert", "Authentication error", JOptionPane.ERROR_MESSAGE)
                }


            }
        });

        def panel = new JPanel();
        panel.setLayout(new GridLayout(5, 2, 10, 10));

        panel.add(lblUrl);
        panel.add(jiraUrl);
        panel.add(lblUser);
        panel.add(tfUser);
        panel.add(lblPassword);
        panel.add(pfPassword);
        panel.add(btnLogin);
        panel.add(btnGet);
        panel.add(lblCert);
        panel.add(cbCert);

        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 200);
        frame.getContentPane().add(panel);
        frame.setVisible(true);
    }

    boolean checkConnection() {
        getRequest(url + "/rest/api/2/user?username=" + account)
        (lastResponseStatus == 200)
    }

    String getRequest(address, method = "GET") {
        try {
            if (disableCert) {
                disableSslForConnection()
            }
            def connection = address.toURL().openConnection()
            connection.addRequestProperty("Authorization", "Basic ${credentials}")
            connection.setRequestMethod(method)
            connection.doOutput = false
            connection.connect()
            lastResponseStatus = connection.responseCode
            def response = connection.content.text
            println "Call ${address} by ${method}, got ${connection.responseCode}"
            println response
            return response
        } catch (Exception e) {
            println e.getMessage()
            e.getStackTrace().each { println it }
        }

    }

    String postRequest(address, body, method = "POST") {
        try {
            if (disableCert) {
                disableSslForConnection()
            }
            def connection = address.toURL().openConnection()
            connection.addRequestProperty("Authorization", "Basic ${credentials}")
            connection.addRequestProperty("Content-Type", "application/json")
            connection.setRequestMethod(method)
            connection.doOutput = true
            connection.outputStream.withWriter {
                it.write(body)
                it.flush()
            }
            connection.connect()
            lastResponseStatus = connection.responseCode
            println "Call ${address} by ${method}, got ${connection.responseCode}"
            if (!lastResponseStatus.toString().startsWith("2")){
                println body
            }
        } catch (Exception e) {
            println e.getMessage()
            e.getStackTrace().each { println it }
        }
    }

    void disableSslForConnection() {
        TrustManager[] trustAllCerts = [
                new X509TrustManager() {
                    @Override
                    void checkClientTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws CertificateException {
                    }

                    @Override
                    void checkServerTrusted(java.security.cert.X509Certificate[] x509Certificates, String s) throws CertificateException {
                    }

                    java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null
                    }
                }
        ]

        def sc = SSLContext.getInstance("SSL")
        sc.init(null, trustAllCerts, new java.security.SecureRandom());

        def allHostsValid = new HostnameVerifier() {
            boolean verify(String hostname, SSLSession session) {
                return true;
            }
        }
        //connection.setDefaultSSLSocketFactory(sc.getSocketFactory())
        //connection.setDefaultHostnameVerifier(allHostsValid)
        HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory())
        HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid)
    }

    String createComponent(JiraComponent jComp) {

        def json = """
        {"name": "${jComp.name}",
        "assigneeType": "${jComp.assigneeType}",
        "project": "${toProjectKey}"
        """
        if (jComp.description != null) {
            json += ",\"description\": \"${jComp.description}\""
        }
        if (jComp.lead != null) {
            json += ",\"leadUserName\": \"${jComp.lead}\""
        }
        json += "}"

        postRequest(url + "/rest/api/latest/component", json)

    }

    String updateComponent(JiraComponent from, JiraComponent to) {
        def descr = ""
        def lead = ""
        if (from.description != null) {
            descr = from.description
        }
        if (from.lead != null) {
            lead = from.lead
        }
        def json = """{
            "assigneeType": "${from.assigneeType}",
            "description": "${descr}",
            "leadUserName": "${lead}"
            }"""

        postRequest(url + "/rest/api/latest/component/${to.id}", json, "PUT")
    }

    String deleteComponent(JiraComponent component) {
        getRequest(url + "/rest/api/2/component/${component.id}", "DELETE")
    }

    void askFromToProjects() {

        def jfProjects = new JFrame("Set projects")
        jfProjects.setLocationRelativeTo(null)
        def lblFromProject = new JLabel("FROM")
        def fromProject = new JTextField()
        lblFromProject.setLabelFor(fromProject)

        def lblToProject = new JLabel("TO")
        def toProject = new JTextField()
        lblToProject.setLabelFor(toProject)

        def jbCompareComponents = new JButton("Compare component")
        def jbUnionComponents = new JButton("Union w/o replace")
        def jbUnionComponentsReplace = new JButton("Union with replace")
        def jbSyncComponents = new JButton("Sync components")

        jbCompareComponents.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {
                if (isFieldsNotEmpty(fromProject, toProject)) {
                    compareComponents(fromProject.getText().toUpperCase(), toProject.getText().toUpperCase())
                }
            }
        })

        jbUnionComponents.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {
                if (isFieldsNotEmpty(fromProject, toProject)) {
                    unionComponentsNoReplace(fromProject.getText().toUpperCase(), toProject.getText().toUpperCase())
                }
            }
        })

        jbUnionComponentsReplace.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {
                if (isFieldsNotEmpty(fromProject, toProject)) {
                    unionComponentsWithReplace(fromProject.getText().toUpperCase(), toProject.getText().toUpperCase())
                }
            }
        })

        jbSyncComponents.addActionListener(new ActionListener() {
            @Override
            void actionPerformed(ActionEvent e) {
                if (isFieldsNotEmpty(fromProject, toProject)) {
                    syncComponents(fromProject.getText().toUpperCase(), toProject.getText().toUpperCase())
                }
            }
        })

        def panel = new JPanel()
        panel.setLayout(new GridLayout(4, 2, 10, 10))
        panel.add(lblFromProject)
        panel.add(lblToProject)
        panel.add(fromProject)
        panel.add(toProject)
        panel.add(jbCompareComponents)
        panel.add(jbUnionComponents)
        panel.add(jbUnionComponentsReplace)
        panel.add(jbSyncComponents)

        jfProjects.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        jfProjects.setSize(400, 300);
        jfProjects.getContentPane().add(panel);
        jfProjects.setVisible(true);
    }

    boolean isFieldsNotEmpty(JTextField fieldFrom, JTextField fieldTo) {
        (!fieldFrom.getText().empty && !fieldTo.getText().empty)
    }

}

