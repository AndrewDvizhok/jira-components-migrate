class JiraProject {
    String projectId
    String projectKey
    String projectName
    JiraProject(id , key, name){
        projectId = id
        projectKey = key
        projectName = name
    }

    @Override
    String toString() {
        return "(${projectKey})${projectName}"
    }
}
